import { expect } from "chai";
import { AuthorController } from "../lib/controllers/author.controller";
const author = new AuthorController();
const forauthor = require("./data/forauthor_testData.json");
var chai = require("chai");
chai.use(require("chai-json-schema"));

describe("Author controller", () => {
let token:string;
let id:string;
let userId:string;
let articleId:string

  it("User login as Author", async () => {
    let response = await author.authenticateUser();
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forauthor.schema_authenticateUser);
    //expect(response.statusCode, `Status Code should be 200`).to.be.equal(400);  negative test
    //console.log(response.body)
    token = response.body.accessToken;
  });

  it("Get Settings for Author", async () => {
    let response = await author.getAuthorSettings(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forauthor.schema_getAuthorSettings)
    //expect(response.body).to.be.jsonSchema(forauthor.schema_authenticateUser)   negative test
    //console.log(response.body)
    id = response.body.id;
    userId = response.body.userId;
  });

  it("Set Settings for Author", async () => {
    let response = await author.setAuthorSettings(token,id,userId);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    //expect(response.body).to.be.jsonSchema(forauthor.schema_authenticateUser)   negative test
    //console.log(response.body)
  });

  it("Get Current User", async () => {
    let response = await author.getCurrentUser(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forauthor.schema_getCurrentUser)
    //console.log(response.body)
  });

  it("Get Public Author", async () => {
    let response = await author.getPublicAuthor(token,id);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forauthor.schema_getPublicAuthor)
    //console.log(response.body)
  });

  it("Save Article", async () => {
    let response = await author.saveArticle(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forauthor.schema_saveArticle)
    //console.log(response.body)
  });

   it("Get Articles", async () => {
    let response = await author.getArticles(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forauthor.schema_getArticles)
    //console.log(response.body)
  });

  it("Get Article by ID", async () => {
    let response = await author.getArticleByID(token,articleId);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    //console.log(response.body)
    articleId = response.body[0].id;
  });

  it("Save Comment", async () => {
    let response = await author.saveComment(token,articleId);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    //console.log(response.body)
  });

   it("Get Comment by Article", async () => {
    let response = await author.getCommentsByArticle(token,articleId);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    //console.log(response.body)
  });
});
