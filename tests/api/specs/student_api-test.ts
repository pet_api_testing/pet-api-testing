import { expect } from "chai";
import { StudentController } from "../lib/controllers/student.controller";
const student = new StudentController();
const forstudent = require("./data/forstudent_testData.json");
var chai = require("chai");
chai.use(require("chai-json-schema"));


describe("Student controller", () => {
  let token:string;
  let id:string;
  it("User login as Student", async () => {
    let response = await student.authenticateUser();
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forstudent.schema_authenticateStudent);
    //console.log(response.body);
    token = response.body.accessToken;
  });

  it("Get Student Settings", async () => {
    let response = await student.getSettingStudent(token,id);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forstudent.schema_getStudentSettings);
    //expect(response.statusCode, `Status Code should be 200`).to.be.equal(400);  negative test
    //console.log(response.body);
  });

  it("Get All Courses", async () => {
    let response = await student.getAllCourses(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    //expect(response.body).to.be.jsonSchema(forstudent.schema_authenticateStudent);  negative test
    //console.log(response.body);
  });

  it("Start Course", async () => {
    let response = await student.startCourse(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forstudent.schema_startCourse);
    //console.log(response.body);
  });

  it("Get Favorite Courses", async () => {
    let response = await student.favoriteCourses(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    //expect(response.body).to.be.jsonSchema(forstudent.schema_startCourse);  negative test
    //console.log(response.body);
  });

  it("Paths for Student", async () => {
    let response = await student.getPaths(token);
    expect(response.statusCode, `Status Code should be 200`).to.be.equal(200);
    expect(response.timings.phases.total, `Response time should be less than 5s`).to.be.lessThan(5000);
    expect(response.body).to.be.jsonSchema(forstudent.schema_pathsForStudent);
    //console.log(response.body);
  });
});