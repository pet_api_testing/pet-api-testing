import { ApiRequest } from "../request";
let baseUrl: string = 'https://knewless.tk/api/'

export class AuthorController {
  async authenticateUser() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`auth/login`)
      .body({ "email": "nixaye1335@altpano.com", "password": "testertester1" })
      .send();
    return response;
  }

  async getAuthorSettings(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`author`)
      .bearerToken(token)
      .send();
    return response;
  }

  async setAuthorSettings(token,id,userId) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`author`)
      .bearerToken(token)
      .body({ 
              "avatar": "string",
              "biography": "bio",
              "company": "BSA",
              "firstName": "Vik",
              "id": id,
              "job": "QA",
              "lastName": "Riashyna",
              "location": "Ukraine",
              "twitter": "twitter.com",
              "userId": userId,
              "website": "website.com"
 })
      .send();
    return response;
  }

  async getCurrentUser(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`user/me`)
      .bearerToken(token)
      .send();
    return response;
  }

  async getPublicAuthor(token,id) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`author/overview/${id}`)
      .bearerToken(token)
      .send();
    return response;
  }

  async saveArticle(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`article`)
      .bearerToken(token)
      .body({
                    "name": "Viktoria Riashyna",
                    "image": "string",
                    "text": "Article",
                    "uploadImage": {}
})
      .send();
    return response;
  }

  async getArticles(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article/author`)
      .bearerToken(token)
      .send();
    return response;
  }

  async getArticleByID(token,articleId) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article/${articleId}`)
      .bearerToken(token)
      .send();
    return response;
  }

  async saveComment(token,articleId) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`article_comment`)
      .bearerToken(token)
      .body({"text": "Comment comment","articleId": articleId})
      .send();
    return response;
  }

  async getCommentsByArticle(token,articleId) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`article_comment/of/${articleId}?size=200`)
      .bearerToken(token)
      .send();
    return response;
    console.log(response.body)
  }

  
}
