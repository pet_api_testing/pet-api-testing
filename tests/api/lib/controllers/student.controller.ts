
import { ApiRequest } from "../request";
let baseUrl: string = 'https://knewless.tk/api/'

export class StudentController {
  async authenticateUser() {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`auth/login`)
      .body({ "email": "tester@datab.info", "password": "testertester1" })
      .send();
    return response;
  }

  async getSettingStudent(token, id) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`student`)
      .bearerToken(token)
      .send();
    return response;
  }

  async settingGoal(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`student/goal`)
      .bearerToken(token)
      .body({"goalId": "b7c80d5e-4e93-428b-b7ca-4436b34b642c"})
      .send();
    return response;
  }

  async getAllCourses(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`course/all`)
      .bearerToken(token)
      .send();
    return response;
    console.log(response.body[10])
  }

  async startCourse(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("POST")
      .url(`course/continue/start`)
      .bearerToken(token)
      .body({"courseId": "cf4cf7e7-362e-4246-bd60-87486cc51f48"})
      .send();
    return response;
  }

  async favoriteCourses(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`favorite/courses`)
      .bearerToken(token)
      .send();
    return response;
  }

    async getPaths(token) {
    const response = await new ApiRequest()
      .prefixUrl(baseUrl)
      .method("GET")
      .url(`paths/student`)
      .bearerToken(token)
      .send();
    return response;
}
}